//
//  main.m
//  BeaconReceiver
//
//  Created by Contract on 10/04/2015.
//  Copyright (c) 2015 Contract. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
